# Pigeon

pigeon is a simple detection system that runs asynchronously in a loop, checks
conditions defined in a YAML file on the prescribed schedule, and outputs to
standard out when it finds something worth noting.

pigeon outputs in a parseable way designed to be picked up by a process
watching the log file to which pigeon writes (e.g. journald). There is no other
notification functionality.

When run without arguments, pigeon runs in the default loop.

When run with the `--debug` argument, pigeon does not run in a loop. Instead it
executes each enabled method once and provides additional output even if the
check is successful.

Optionally, you can pass one or more more `--method` arguments to limit a run
to just the methods chosen.
