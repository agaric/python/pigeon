backupninja:
  enabled: true
  interval: .4

disk_usage:
  enabled: true
  interval: .2
  checks:
    - percent: 60
      priority: critical
    - percent: 30
      path: /srv
      priority: warning

systemd:
  enabled: true
  interval: .1

ntpsync:
  enabled: true
  interval: .1

mailq:
  enabled: true
  interval: .1
  checks:
    - count: 100
      priority: warning
      path: /var/spool/postfix
    - count: 500
      priority: critical
      path: /var/spool/postfix

# These are the smartctl attributes we check. By default we expect
# the count to be 0 for all of them.
#
# SMART ID 5 (0x05): Relocated Sectors Count
# SMART ID 187 (0xBB): Reported Uncorrectable Errors
# SMART ID 188 (0xBC): Command Timeout
# SMART ID 197 (0xC5): Current Pending Sector Count
# SMART ID 198 (0xC6): Uncorrectable Sector Count
#
# If you have a non-zero count for any of them, you can override
# the threshhold for an alert by using the id number as a key in
# the count_exceptions dict.
smartctl:
  enabled: true
  inverval: 60
  checks:
   - path: /dev/sda
     count_exceptions:
       # Make 200 the threshhold for the smartctl attribute with id 5.
       # For all others it will be 0, the default.
       5: 200
   - path: /dev/sdb

apt:
  enabled: true
  # Number of days there can be pending upgrades before we send a warning.
  threshhold: 30
  interval: 60

filter_check:
  enabled: true
  # List of providers to test, must match filter-check.conf blocks.
  sendtos: [ "gmail", "yahoo", "outlook" ]
  interval: 120
  emailfroms: [ "one@address.org", "another@address.org", "andathird@address.org" ]
  subjects: [ "something realistic", "another likely subject line", "not something that sounds like a spammer" ]
  msgs:
    - |
      Dear Joe,

      This is a realistic sounding body of an email.
    - |
      Yes, this really is an email that I might send.
      Your truly,
      Bob
    - |
      And just for variety, here is a third email body

mistert_audit_relayers_15:
  method: mistert_audit_relayers
  enabled: true
  interval: 300 

mistert_audit_relayers_120:
  method: mistert_audit_relayers
  enabled: true
  interval: 7200 

oomkiller:
  method: oomkiller
  enabled: true
  interval: 600
